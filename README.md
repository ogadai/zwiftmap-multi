# ZwiftMap

**ZwiftMap - A multi platform map and tracker for Zwift**


## To Use

Download and install the application.

Start the installed application.

Minimise the configuration window to only see map and/or chat log.

Exit the application by closing the configuration window (in Windows) or by selecting Quit in the menu (in macOS).


## License
[CC0 1.0 (Public Domain)](LICENSE.md)

This license applies only to the original parts of the work. The original licenses of any included packages apply without modification.


## History

#### 1.1.2 2017-04-01

* Now the map can automatically center on your position. Use the Start follow me / Stop follow me button in the map window.

#### 1.1.1 2017-03-31

This version is released only for Windows:

* Bug fix: Toggle fullscreen did not work in the released version 1.1.0


#### 1.1.0 2017-03-31

Let me start by praising Andy Lee (zwiftgps.com) for his contributions. All of the hardcore changes in ZwiftMap which allows us to retrieve information from Zwift, show rider data etc. is his work.

The big news:

If you sign in with your Zwift user you will get a lot of extra features:
* User names instead of user IDs in the chat log.
* See the position of the riders you follow directly on the map.
* Launch your own local ZwiftGPS server - then you can see the map in a browser from any device in your network!
* Race ghosts - either your own previous activities or those of other riders.

Do you not want to see those you follow on the map but still would like to see user names in the chat log? Then check the option 'Show only my position (not followees)' - then ZwiftMap will show just show your own position.

* The interaction mode (where you can drag and resize a window) has been much improved. You can now resize by pulling on any of the four borders.
* You can zoom and pan in the map window. Simply press the 'Enable Pan/Zoom' button when the map window is in interaction mode. The setting can be remembered (this is a per world setting) between sessions. To return to default pan and zoom in future session press first 'Reset Zoom/Pan' and then 'Remember Zoom/Pan'.
* Toggle pseudo fullscreen mode for Zwift (Windows only).

There is online help in the configuration, map, and chat log windows. Hover over or click the ?-mark icons to get further descriptions of the different features.



#### 1.0.0 2017-02-07

Version 1.0.0 is a major update with more features:

* A new main window for configuration and activating/deactivating windows is introduced.
* A new chat log window is introduced.
* You can choose if you want to see the map and/or the chat log. This configuration can be changed via the configuration window, and can be saved so it is remembered the next time you run ZwiftMap.
* The map and chat windows can be resized by pulling on their bottom and right borders.
* Window sizes and positions are saved between sessions. The saved positions can be deleted via the configuration window if you want a fresh start.
* There is a button in the configuration window to toggle window state (same effect as the magic key combo).
* You can override the default colours for the map and the rider marker. The chosen colours are remembered between sessions if you save the configuration.
* You can manually override which map is shown (Watopia, Richmond, or London) from the configuration. You can also just start ZwiftMap _before_ Zwift because it then will detect from the Zwift log file which course is loaded in Zwift and switch to the correct map.
* The map is now styled in a way which should make it nicer to look at. Altitude is indicated by the colour of each road segment (most clearly seen in the Watopia map -- the other courses have much less altitude variations). 
* Accepts a command line argument (--window) to have the map shown in a normal window. This window can be captured
by streaming software such as OBS Studio.

Other possibilities with this new version (probably not needed by most users):

* Style the map with a user.css file. Save your styling as user.css in Documents/zwift/zwiftmap

#### 0.2.0 2017-01-18

* All platforms:
  * Changed the method to determine the location of the Documents folder. This hopefully
makes the app robust to situations where the Documents folder has been moved to
a non default location (issue reported by a Windows user).
* Mac:
  * There is a new menu item which does the same as the magic key combo. It toggles whether the
map window responds to mouse events or not.


#### 0.1.1 2017-01-16
* A dotted frame is shown around map when it can be moved with the mouse.

#### 0.1.0 2017-01-15
* Initial release


## Acknowledgements

Let me start by praising Andy Lee (zwiftgps.com) for his contributions. All of the hardcore changes in ZwiftMap which allows us to retrieve information from Zwift, show rider data etc. is his work.

### Built on

* [electron](http://electron.atom.io)
* [node.js](http://nodejs.org)
* [always-tail](https://www.npmjs.com/package/always-tail)
* [jquery](https://jquery.com/)
* [has-flag](http://www.npmjs.com/package/has-flag)
* [electron-window-state](https://github.com/mawie81/electron-window-state)
* [electron-config](https://github.com/sindresorhus/electron-config)
* [zwift-mobile-api](https://www.npmjs.com/package/zwift-mobile-api)
* [zwift-second-screen](https://www.npmjs.com/package/zwift-second-screen)
* [font-awesome](http://fontawesome.io)

### Built with

* [electron-packager](https://www.npmjs.com/package/electron-packager)
* [electron-installer-dmg](https://www.npmjs.com/package/electron-installer-dmg)
* [inno-setup]
