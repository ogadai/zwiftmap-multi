﻿const EventEmitter = require('events')
const POLL_INTERVAL = 2000;

const is = require('electron-is');
const log = require('electron-log');

// if (is.dev) {
//   log.transports.file.level = 'info';
//   log.transports.console.level = 'info';
// } else {
//   log.transports.file.level = 'error';
//   log.transports.console.level = false;
// }

// Proxy location data from either the log-data source
// or the Zwift API source (user signed in)
class LocationData extends EventEmitter {
  constructor(source) {
    super();

    this.world = null
    this.subscriptions = 0;
    this.interval = null;

    this.handlePositions = this.handlePositions.bind(this);

    this.setSource(source);
  }

  setSource(source) {
    this.detatchHandlers();
    this.source = source;
    this.attachHandlers();
  }

  setWorld(world) {
    this.world = world
    this.emit('world', this.world)
  }

  getWorld() {
    return this.world;
  }

  getProfile() {
    return this.source.getProfile();
  }

  getPositions() {
    return this.source.getPositions();
  }

  getRiders() {
    return this.source.getRiders
        ? this.source.getRiders()
        : Promise.resolve([]);
  }

  getGhosts() {
    return this.source.getGhosts
        ? this.source.getGhosts()
        : { getList: () => null }
  }

  getActivities(worldId, playerId) {
    return this.source.getGhosts
        ? this.source.getActivities(worldId, playerId)
        : Promise.resolve([]);
  }

  regroupGhosts() {
    return this.source.regroupGhosts
        ? this.source.regroupGhosts()
        : null;
  }

  attachHandlers() {
    if (this.source) {
      this.source.on('positions', this.handlePositions);
    }
  }

  detatchHandlers() {
    if (this.source) {
      this.source.removeListener('positions', this.handlePositions);
    }
  }

  handlePositions(positions) {
    this.emit('positions', positions);
    log.verbose(positions);
  }

  subscribe() {
    // Subscribing triggers an interval that calls the
    // pollPositions method (if it exist). For the API
    // version this gets a position update, but for the
    // log version it's ignored
    if (this.subscriptions === 0) {
      console.log(`start-polling`);
      const pollFn = () => {
        if (this.source.pollPositions) this.source.pollPositions();
      }
      this.interval = setInterval(pollFn, POLL_INTERVAL);
      pollFn();
    }
    this.subscriptions++;

    let unsubscribed = false;
    return () => {
      if (!unsubscribed) {
        unsubscribed = true;
        this.subscriptions--;

        if (this.subscriptions === 0) {
          console.log(`stop-polling`);
          clearInterval(this.interval);
        }
      }
    }
  }
}

module.exports = LocationData
