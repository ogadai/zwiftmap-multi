i18n = require('i18n');

i18n.configure({
    locales:['en'],
	defaultLocale:'en',
	updateFiles:true,
	prefix: 'helpify-',
	objectNotation: true,
    directory: __dirname + '/assets/locales'
});


module.exports = {


__: (element) => {
	var newNodeIcon = document.createElement('div');
	var newNodeText = document.createElement('div');

	// console.log(element.getAttribute('data-help'));

	var pattern = /([^\|]+)(\|fixed|\|tooltip)*/i
	var match = pattern.exec(element.getAttribute('data-help'));
	// console.log(match);
	var name = match[1]
	var tooltip = ((match[2]) && (match[2] == '|tooltip'))
	var fixed = ((match[2]) && (match[2] == '|fixed'))
	// console.log(match)
	// console.log('match', name, tooltip, fixed)

	newNodeText.innerHTML = i18n.__(name).replace(/[\n]/g,'<br />');
	console.log(newNodeText.innerHTML);
	if (tooltip) {
		newNodeIcon.className = 'helpify helpify-tooltip';
	} else {
		newNodeIcon.className = 'helpify';
	}
	newNodeText.className = 'helpify-text';
	newNodeText.hidden = true;
	newNodeIcon.innerHTML = '<i class="fa fa-question-circle" aria-hidden="true"></i>';
	if (tooltip) {
		newNodeIcon.style.display = 'inline-block';
		newNodeIcon.appendChild(newNodeText);
		element.appendChild(newNodeIcon);
	} else if (fixed) {
		newNodeIcon.style.display = 'inline-block';
		element.ownerDocument.querySelector(element.getAttribute('data-help-anchor')).appendChild(newNodeText);
		element.appendChild(newNodeIcon);
	} else {
		switch (window.getComputedStyle(element).getPropertyValue("display")) {
		// case 'div': 
		case 'block': 
			newNodeIcon.style.display = 'inline-block';
			element.appendChild(newNodeIcon);
			element.parentNode.insertBefore(newNodeText, element.nextSibling);
			break;
		default: 
			newNodeIcon.style.display = 'inline-block';
			// element.style.display = 'inline';
			element.parentNode.insertBefore(newNodeIcon, element.nextSibling);
			element.parentNode.insertBefore(newNodeText, newNodeIcon.nextSibling);
			break;
		}
	}

	if ((tooltip) || (fixed)) {
		newNodeIcon.addEventListener('mouseover', () => {
			newNodeText.hidden = false;
		})
		newNodeIcon.addEventListener('mouseout', () => {
			newNodeText.hidden = true;
		})
	} else {
		newNodeIcon.addEventListener('click', () => {
			newNodeText.hidden = !newNodeText.hidden;
		})

	}

	newNodeText.addEventListener('click', () => {
		newNodeText.hidden = !newNodeText.hidden;
	})

}


};

