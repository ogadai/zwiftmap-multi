// This file is required by the chat.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const Config = require('electron-config');
const config = new Config();

const electron = require('electron')
const {ipcRenderer} = require('electron')

const remote = require('electron').remote

const os = require('os')

var color = remote.getGlobal('color')

const opn = require('opn')

const hpfy = require('./helpify.js');

const log = require('electron-log')

const is = require('electron-is');

/*

const app = require('electron').remote.app
const fs = require('fs')
const helpers = require('./helpers.js')

usercss = app.getPath('documents') + "/Zwift/zwiftmap/user.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

*/



var buttonSave = document.querySelector('#button-save');
var buttonDelete = document.querySelector('#button-delete');
var buttonResetWindows = document.querySelector('#button-reset-windows');
var buttonToggleWindows = document.querySelector('#button-toggle-windows');
var inputShowMap = document.querySelector('#show-map')
var inputShowNetwork = document.querySelector('#show-network')
var inputShowStat = document.querySelector('#show-stat')
var inputShowChat = document.querySelector('#show-chat')
var inputShowJustMe = document.querySelector('#show-just-me')
var inputRunServer = document.querySelector('#run-server')
var inputServerPort = document.querySelector('#server-port')
var inputServerAddr = document.querySelector('#server-addr')

var inputUseDefaultColor = document.querySelector('#use-default-color')
var inputMapColor = document.querySelector('#map-color')
var inputRiderColor = document.querySelector('#rider-color')
var inputMeColor = document.querySelector('#me-color')
var inputAsOverlay = document.querySelector('#as-overlay')
var inputWindowMode = document.querySelector('#window-mode')
var inputDisableHWAcceleration = document.querySelector('#disable-hw-acceleration')
var buttonWorld1 = document.querySelector('#world1')
var buttonWorld2 = document.querySelector('#world2')
var buttonWorld3 = document.querySelector('#world3')

var buttonNextDisplay = document.querySelector('#next-display')
var buttonPreviousDisplay = document.querySelector('#previous-display')
var buttonGatherWindows = document.querySelector('#gather-windows')
var buttonDefaultPositions = document.querySelector('#default-positions')

var buttonFullscreen = document.querySelector('#button-fullscreen')

buttonFullscreen.disabled = !is.windows()

var captionSignIn = document.querySelector('#caption-sign-in')
var buttonSignIn = document.querySelector('#button-sign-in')

var buttonEditGhosts = document.querySelector('#button-edit-ghosts')

// read config (will initialise inputShowMap, inputShowChat and inputRunServer
readConfig()

buttonSave.addEventListener('click', () => {
  saveConfig()
})

buttonDelete.addEventListener('click', () => {
  deleteConfig()
})

buttonResetWindows.addEventListener('click', () => {
  console.log('buttonResetWindows')
  resetWindows()
})

buttonToggleWindows.addEventListener('click', () => {
  console.log('buttonToggleWindows')
  ipcRenderer.send('toggle-window-state')
})

buttonWorld1.addEventListener('click', () => {
  ipcRenderer.send('set-world', 1)
})

buttonWorld2.addEventListener('click', () => {
  ipcRenderer.send('set-world', 2)
})

buttonWorld3.addEventListener('click', () => {
  ipcRenderer.send('set-world', 3)
})

var signedIn = false;
buttonSignIn.addEventListener('click', () => {
  if (signedIn) {
    signedIn = false
    buttonSignIn.innerHTML = 'Sign in'
    captionSignIn.innerHTML = 'Sign in to Zwift'
    ipcRenderer.send('sign-out')

    updateGhostsButton()
  } else {
    ipcRenderer.send('begin-sign-in')
  }
})

ipcRenderer.on('signed-in', (event, name) => {
  signedIn = true
  buttonSignIn.innerHTML = 'Sign out'
  captionSignIn.innerHTML = `Signed in as ${name}`

  updateGhostsButton()
})

function updateGhostsButton() {
    buttonEditGhosts.disabled = !signedIn || !inputRunServer.checked
}

buttonEditGhosts.addEventListener('click', () => {
  ipcRenderer.send('edit-ghosts')
})

inputShowMap.addEventListener('change', () => {
    showMap = inputShowMap.checked
    console.log('caught change event')
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-map', showMap, asOverlay)
})

inputShowChat.addEventListener('change', () => {
    showChat = inputShowChat.checked
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-chat', showChat, asOverlay)
})

inputShowStat.addEventListener('change', () => {
    showStat = inputShowStat.checked
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-stat', showStat, asOverlay)
})

buttonNextDisplay.addEventListener('click', () => {
  let mainWindow = remote.getCurrentWindow()
  // console.log(mainWindow)
  let mainDisplay = electron.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})
  // console.log(mainDisplay)
  // alert("Currently: " + mainDisplay.id + " " + mainDisplay.bounds.x + " "  + mainDisplay.bounds.y)
  let allDisplays = electron.screen.getAllDisplays()
  // alert(allDisplays.length)
  allDisplays.push(allDisplays[0])
  // alert(allDisplays.length)
  nextDisplayIndex = 1 + allDisplays.findIndex((element) => {return (element.id == mainDisplay.id)})
  // alert(nextDisplayIndex + "  " + allDisplays[nextDisplayIndex].id)
  var x = allDisplays[nextDisplayIndex].bounds.x + (allDisplays[nextDisplayIndex].bounds.width - mainWindow.getBounds().width) / 2
  var y = allDisplays[nextDisplayIndex].bounds.y + (allDisplays[nextDisplayIndex].bounds.height - mainWindow.getBounds().height) / 2
  
  x = Number(x.toFixed())
  y = Number(y.toFixed())
  
  // alert(mainWindow.getBounds().x +" ; " + mainWindow.getBounds().y + " --> " + x  + " ; " + y )
  
  mainWindow.setPosition(x, y)
})

buttonPreviousDisplay.addEventListener('click', () => {
  let mainWindow = remote.getCurrentWindow()
  let mainDisplay = electron.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})
  // console.log(mainDisplay)
  let allDisplays = electron.screen.getAllDisplays().reverse()
  allDisplays.push(allDisplays[0])
  nextDisplayIndex = 1 + allDisplays.findIndex((element) => {return (element.id == mainDisplay.id)})
  
  var x = allDisplays[nextDisplayIndex].bounds.x + (allDisplays[nextDisplayIndex].bounds.width - mainWindow.getBounds().width) / 2
  var y = allDisplays[nextDisplayIndex].bounds.y + (allDisplays[nextDisplayIndex].bounds.height - mainWindow.getBounds().height) / 2
  
  x = Number(x.toFixed())
  y = Number(y.toFixed())
  
  // alert(mainWindow.getBounds().x +" ; " + mainWindow.getBounds().y + " --> " + x  + " ; " + y )

  mainWindow.setPosition(x, y)
})

buttonDefaultPositions.addEventListener('click', () => {
  ipcRenderer.send('default-positions')
})

buttonGatherWindows.addEventListener('click', () => {
  ipcRenderer.send('gather-windows')
})
  
function updateServer() {
  runServer = inputRunServer.checked
  serverPort = parseInt(inputServerPort.value)

  // do some stuff based on value
  ipcRenderer.send('run-server', runServer, serverPort)
  updateGhostsButton()
}

function getHostName() {
  return os.hostname() || 'localhost'
}

function clickedServer() {
  var addr = 'http://' + getHostName() + ':' + parseInt(inputServerPort.value)
  require('opn')(addr)
}

inputRunServer.addEventListener('change', updateServer)
inputServerPort.addEventListener('change', updateServer)
inputServerAddr.addEventListener('click', clickedServer)
inputServerAddr.innerText = 'http://' + getHostName() + ':'

inputShowNetwork.addEventListener('change', () => {
    showNetwork = inputShowNetwork.checked
    // do some stuff based on value
    asOverlay = inputAsOverlay.checked
    ipcRenderer.send('show-network', showNetwork, asOverlay)
})


inputShowJustMe.addEventListener('change', () => {
    showJustMe = inputShowJustMe.checked
    // do some stuff based on value
    var track = { 'me': true, 'followees': !showJustMe }
    ipcRenderer.send('change-trackers', track)
})


inputUseDefaultColor.addEventListener('change', () => {
    useDefaultColor = inputUseDefaultColor.checked
    // do some stuff based on value
    //ipcRenderer.send('xxxxxx', useDefaultColor)
})

inputAsOverlay.addEventListener('change', () => {
    asOverlay = inputAsOverlay.checked
    // do some stuff based on value
    //ipcRenderer.send('xxxxxx', useDefaultColor)
    showMap = inputShowMap.checked
    showChat = inputShowChat.checked
    showStat = inputShowStat.checked
    ipcRenderer.send('show-network', showNetwork, asOverlay)
    ipcRenderer.send('show-stat', showStat, asOverlay)
    ipcRenderer.send('show-chat', showChat, asOverlay)
    ipcRenderer.send('show-map', showMap, asOverlay)
})

inputWindowMode.addEventListener('change', () => {
  //
})

inputDisableHWAcceleration.addEventListener('change', () => {
  //
})

buttonFullscreen.addEventListener('click', () => {
  if (is.windows()) {
    opn(remote.app.getAppPath() + '\\helpers\\pseudofullscreen.exe');
  }
})

document.querySelector('#loglevel').addEventListener('change', () => {
  ipcRenderer.send('set-log-level', document.querySelector('#loglevel').value)
}) 

Array.from(document.querySelectorAll('#background button')).forEach( (b) => {
  b.addEventListener('click', (event) => {
    let opacity = event.currentTarget.getAttribute('value');
    backgroundOpacity(opacity)
  })
})



// ---

window.addEventListener('load', () => {
    var elems = document.querySelectorAll('[data-help]')
    // var values =
    elems.forEach( (obj) => {
      // alert(obj.innerHTML);
      hpfy.__(obj);
      // return obj.value;
    });


    document.querySelectorAll('[data-platform="' + process.platform + '"]').forEach( (obj) => {
      obj.hidden = false;
    })
})


// Function definitions after this point ***************

function saveConfig () {
  config.set('show.map', inputShowMap.checked)
  config.set('show.chat', inputShowChat.checked)
  config.set('show.stat', inputShowStat.checked)
  config.set('show.network', inputShowNetwork.checked)
  config.set('show.justme', inputShowJustMe.checked)
  config.set('server.run', inputRunServer.checked)
  config.set('server.port', inputServerPort.value)

  opacity = document.querySelector('#background button.active').getAttribute('value')
  config.set('background.opacity', opacity)
  // ipcRenderer.send('change-background', '' + opacity/100)

  config.set('color.use-default-color', inputUseDefaultColor.checked)
  config.set('color.map', inputMapColor.value)
  config.set('color.rider', inputRiderColor.value)
  config.set('color.me', inputMeColor.value)
  config.set('mode.as-overlay', inputAsOverlay.checked)
  config.set('mode.window', inputWindowMode.checked)
  config.set('graphics.disable-hw-acceleration', inputDisableHWAcceleration.checked)

  color = config.get('color')
  ipcRenderer.send('change-color', color) 


} // saveConfig

function deleteConfig () {
  config.clear()
} // deleteConfig

function readConfig () {

  showMap = (config.has('show.map') ? config.get('show.map') : true)
  showChat = (config.has('show.chat') ? config.get('show.chat') : true)
  // showStat defaults to false
  showStat = (config.has('show.stat') ? config.get('show.stat') : false)
  // showNetwork defaults to false until fully developed
  showNetwork = (config.has('show.network') ? config.get('show.network') : false)
  // showJustMe defaults to false
  showJustMe = (config.has('show.justme') ? config.get('show.justme') : false)

  runServer = (config.has('server.run') ? config.get('server.run') : false)
  serverPort = (config.has('server.port') ? config.get('server.port') : 8080)
  opacity = (config.has('background.opacity') ? config.get('background.opacity') : 0)
  useDefaultColor = (config.has('color.use-default-color') ? config.get('color.use-default-color') : true)
  mapColor = (config.has('color.map') ? config.get('color.map') : '#e35205')
  riderColor = (config.has('color.rider') ? config.get('color.rider') : '#0080ff')
  meColor = (config.has('color.me') ? config.get('color.me') : '#ffffff')
  asOverlay = (config.has('mode.as-overlay') ? config.get('mode.as-overlay') : true)
  windowMode = (config.has('mode.window') ? config.get('mode.window') : false)
  disableHWAcceleration = (config.has('graphics.disable-hw-acceleration') ? config.get('graphics.disable-hw-acceleration') : false)

  // login
  signIn = (config.has('login.save') ? config.get('login.save') : false)
  if (signIn && config.has('login.uername') && config.has('login.password')) {
    ipcRenderer.send('begin-silent-sign-in')
    ipcRenderer.send('sign-in', config.get('login.uername'), config.get('login.password'))
  }

  if (remote.getGlobal('mode.window')) {
    // override configuration if global mode.as-window is true (e.g. if started with flag --window)
    windowMode = true
  }

  if (windowMode) {
    // forced windowMode overrides asOverlay
    asOverlay = false
  }

  inputAsOverlay.checked = asOverlay
  inputWindowMode.checked = windowMode
  inputDisableHWAcceleration.checked = disableHWAcceleration

  inputShowJustMe.checked = showJustMe
  track = { 'me': true, 'followees': !showJustMe }
  ipcRenderer.sendSync('change-trackers', track)
  console.log(remote.getGlobal('track'))

  backgroundOpacity(opacity)

  inputUseDefaultColor.checked = useDefaultColor
  inputMapColor.value = mapColor
  inputRiderColor.value = riderColor
  inputMeColor.value = meColor

  color = {
    'use-default-color': useDefaultColor ,
    'map': mapColor,
    'rider': riderColor,
    'me': meColor
  }
  ipcRenderer.sendSync('change-color', color) 
  console.log(remote.getGlobal('color'))

  inputShowMap.checked = showMap
  ipcRenderer.send('show-map', showMap, asOverlay)

  inputShowChat.checked = showChat
  ipcRenderer.send('show-chat', showChat, asOverlay)

  inputShowStat.checked = showStat
  ipcRenderer.send('show-stat', showStat, asOverlay)

  inputShowNetwork.checked = showNetwork
  ipcRenderer.send('show-network', showNetwork, asOverlay)

  inputRunServer.checked = runServer
  inputServerPort.value = serverPort
  ipcRenderer.send('run-server', runServer, serverPort)


  document.querySelector('#output-config-file').innerHTML = '<a href="' + config.path + '" target="_blank">Configuration file</a>';
  document.querySelector('#open-config-file').addEventListener('click', () => {
    if (is.windows()) {
      opn(config.path, {app: ['notepad']});
    } else if (is.macOS()) {
      opn(config.path, {app: ['textedit']});
    }
  })

  document.querySelector('#output-log-file').innerHTML = '<a href="' + remote.getGlobal('log').file + '" target="_blank">Log file</a>';
  document.querySelector('#open-log-file').addEventListener('click', () => {
    if (is.windows()) {
      opn(remote.getGlobal('log').file, {app: ['notepad']});
    } else if (is.macOS()) {
      opn(remote.getGlobal('log').file, {app: ['textedit']});
    }
  })

} // readConfig


function backgroundOpacity(opacity) {
  Array.from(document.querySelectorAll('#background button')).forEach( (b) => {
    b.className = 'ddd btn btn-default'
  })
  
  document.querySelector('button#background-' + opacity).className = 'btn btn-default active'
  
  ipcRenderer.send('change-background', '' + opacity/100) 
  
}



function resetWindows () {
  ipcRenderer.send('clear-window-state')
} // resetWindows



// Additional event handlers after this point *************

// ipc messages to handle:
ipcRenderer.on('config-do-this-do-that', (event, message) => {
  // console.log(arg) // prints message
})

