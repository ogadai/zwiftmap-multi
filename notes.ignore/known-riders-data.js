﻿// Cache rider names if logged in to zwift (account not null)
// See zwift-mobile-api for account definition

class KnownRiders {
  constructor(account) {

    this.riders = [];

    // this.getRiderName = this.getRiderName.bind(this);
    // this.cacheRider = this.cacheRider.bind(this);

    this.setAccount(account);
  }

  setAccount(account) {
    this.account = account;
  }

  getRiderName(riderID) {
    this.cacheRider(riderID);
    if (this.riders && this.riders[riderID]) {
      // found in cache
      //return this.riders[riderID].firstName + ' ' + this.riders[riderID].lastName;
      this.riders[riderID].profile().then(p => {
        const pp = Promise.resolve(p)
        console.log('getRiderName : ' + p.firstName + ' ' + p.lastName);
        return p.firstName + ' ' + p.lastName;
      });
 
    } else {
      return null;
    }
  }

  cacheRider(riderID) {
    if (this.riders && this.riders[riderID]) {
      // already cached
    } else if (this.account) {
      var profile = this.account.getProfile(riderID)
      this.riders[riderID] = profile;

      profile.profile().then(p => {
        console.log('XXXXXXXXXXXXXXXXXXXXX ' + p.firstName);
        console.log('');
        // console.log(p);
      });

      // this.account.getProfile(riderID).profile().then(p => {
      //   console.log(p.firstName);
      //   this.riders[riderID] = p;
      // });
      console.log(this.riders[riderID])
    }
  }

}

module.exports = KnownRiders
