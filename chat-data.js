﻿const EventEmitter = require('events')

class ChatData extends EventEmitter {
  /**
   * 
   * @param {ZwiftAccount} account 
   */
  constructor(account) {
    super()

    this.players = [];

    // this.getRiderName = this.getRiderName.bind(this);
    // this.cachePlayer = this.cachePlayer.bind(this);

    this.setAccount(account);
  }


  /**
   * 
   * @param {ZwiftAccount} account 
   */
  setAccount(account) {
    this.account = account;
  }


  /**
   * 
   * @param {*} playerID 
   */
  cachePlayer(playerID) {
    if (this.players && this.players[playerID]) {
      // already cached
    } else if (this.account) {
      var profile = this.account.getProfile(playerID)
      this.players[playerID] = profile;

      profile.profile().then(p => {
        console.log('cachePlayer: stored ' + playerID + ' = ' + p.firstName);
      });
    }
  }

  /**
   * 
   * @param {*} timestamp 
   * @param {*} playerID 
   * @param {*} message 
   */
  newMessage(timestamp, playerID, message) {
    if (this.account) {
      // use mobile api to look up name
      this.cachePlayer(playerID);
      if (this.players && this.players[playerID]) {
        // found in cache
        this.players[playerID].profile().then(p => {
          // console.log('newMessage : ' + p.firstName + ' ' + p.lastName);

          this.emit('message', [{
            timestamp: timestamp,
            playerID: playerID,
            message: message,
            firstName: p.firstName,
            lastName: p.lastName
            }]) // emit
        });
      }
    } else {
      // this.account is null or undefined
      // don't look for name if zwift account is null
      this.emit('message', [{
        timestamp: timestamp,
        playerID: playerID,
        message: message,
        firstName: '',
        lastName: ''
        }])
    }
  }    
 

}

module.exports = ChatData
