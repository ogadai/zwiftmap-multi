// This file is required by the stat.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const app = require('electron').remote.app
const fs = require('fs')
const helpers = require('./helpers.js')

const Config = require('electron-config');
const config = new Config();

const hpfy = require('./helpify.js');

const is = require('electron-is');
const log = require('electron-log');

usercss = app.getPath('documents') + "/Zwift/zwiftmap/user.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

let statStyles = helpers.inlinecss()

// let stat = { zoom: (config.has('stat.zoom') ? config.get('stat.zoom') : 100) };
// // changeZoom(stat.zoom)
// console.log(`zoom is ${stat.zoom}`);

// supported fields
let fields = ['speed','average','distance', 'climbing', 'power', 'wkg', 'cadence', 'time', 'rideons', 'tempo'];
// initialise defaults and read stored settings
let settings = readConfig();
// initial metrics
let lastStat = {speed: 0, average: 0, distance: 0, climbing: 0, time: 0, power: 0, cadenceUHz: 0, rideOns: 0, sport: 0}

const {ipcRenderer} = require('electron')

// ipc messages to handle:
// new-stat
ipcRenderer.on('new-stat', (event, statJSON) => {
  // console.log(timestamp, user, message, firstName, lastName) // prints message
  var stat = JSON.parse(statJSON);
  log.info(stat);
  document.querySelector('#stat').innerHTML = renderStat(stat, settings.metric);
  lastStat = stat;
})

function renderStat(stat, metric) {
  if (metric == undefined) metric = true;

  var s = "";
  // console.log(stat);
  fields.forEach( (field) => {
    if (settings.show[field]) {
      s = s + renderField(stat, field, metric)
    } 
  })
  
  return s;
}

function renderField(stat, field, metric) {
  switch (field)   {
    case 'speed':
      if (metric) {
        return `<div class="item"><div id="speed" class="value digit-5">${round(stat.speed / 1000000, 1).toFixed(1)}</div><div class="unit">KPH</div></div>`
      } else {
        return `<div class="item"><div id="speed" class="value digit-5">${round(0.621371 * stat.speed / 1000000, 1).toFixed(1)}</div><div class="unit">MPH</div></div>`
      }
      break;
    case 'average':
      if (metric) {
        return `<div class="item"><div id="average" class="value digit-5">${round((stat.time > 0 ? stat.distance / 1000 / (stat.time/60/60) : 0), 1).toFixed(1)}</div><div class="unit">AVG</div></div>`
      } else {
        return `<div class="item"><div id="average" class="value digit-5">${round((stat.time > 0 ? 0.621371 * stat.distance / 1000 / (stat.time/60/60) : 0), 1).toFixed(1)}</div><div class="unit">AVG</div></div>`
      }
      break;
    case 'distance':
      if (metric) {
        return `<div class="item"><div id="distance" class="value digit-5">${round(stat.distance / 1000, 1).toFixed(1)}</div><div class="unit">KM</div></div>`
      } else {
        return `<div class="item"><div id="distance" class="value digit-5">${round(0.621371 * stat.distance / 1000, 1).toFixed(1)}</div><div class="unit">M</div></div>`
      }
      break;
    case 'climbing':
      if (metric) {
        return `<div class="item"><div id="climbing" class="value digit-4">${stat.climbing}</div><div class="unit">M</div></div>`
      } else {
        return `<div class="item"><div id="climbing" class="value digit-4">${(stat.climbing*3.2808399).toFixed(0)}</div><div class="unit">FT</div></div>`
      }
      break;
    case 'power':
      if (stat.sport && stat.sport.low == 1) {
        return ''
      } else {
        return `<div class="item"><div id="power" class="value digit-4">${stat.power}</div><div class="unit">W</div></div>`
      }
      break;
    case 'weight':
      break;
    case 'cadence':
      if (stat.sport && stat.sport.low == 1) {
        return ''
      } else {
        return `<div class="item"><div id="cadence" class="value digit-3">${round(60*stat.cadenceUHz/1000000,0)}</div><div class="unit">RPM</div></div>`
      }
      break;
    case 'tempo':
      if (stat.sport && stat.sport.low == 1) {
        if (metric) {
          return `<div class="item"><div id="cadence" class="value digit-5">${runningCadence(stat.speed)}</div><div class="unit">/KM</div></div>`
        } else {
          return `<div class="item"><div id="cadence" class="value digit-5">${runningCadence(0.621371 * stat.speed)}</div><div class="unit">/M</div></div>`
        }
      } else {
        return ''
      }
    case 'wkg':
      if (stat.sport && stat.sport.low == 1) {
        return ''
      } else {
        return `<div class="item"><div id="wkg" class="value digit-3">${(stat.weight && stat.weight > 0 ? round(stat.power / (stat.weight / 1000),1).toFixed(1) : "-&nbsp;")}</div><div class="unit">W/KG</div></div>`
      }
      break;
    case 'rideons':
      return `<div class="item"><div id="rideOns" class="value digit-3">${stat.rideOns}</div><div class="unit">ride ons</div></div>`
      break;
    case 'time':
      return `<div class="item"><div id="time" class="value digit-8">${timestamp(stat.time)}</div><div class="unit">ET</div></div>`
      break;
    default:
      return "";
      break;
   }

}

fields.forEach( (field) => {
  document.querySelector('#show-' + field).addEventListener('change', (event) => {
    settings.show[event.srcElement.getAttribute('data-show')] = event.srcElement.checked;
    document.querySelector('#stat').innerHTML = renderStat(lastStat, settings.metric);
  })
})

document.querySelector('#show-units').addEventListener('change', (event) => {
  settings.show[event.srcElement.getAttribute('data-show')] = event.srcElement.checked;
  if (event.srcElement.checked) {
    $('#units').show()
  } else {
    $('#units').hide()
  }
})

ipcRenderer.on('ignore-mouse', (event, ignoreMouseEvents) => {
  if (ignoreMouseEvents) {
      // hide visual indicators
      $('html').removeClass('allow_interaction')
    } else {
      // show visual indicators that this window accepts mouse events
      $('html').addClass('allow_interaction')
  }
})


// document.querySelector('#remember').addEventListener('click', () => {
//   stat.zoom = document.querySelector('#zoom').value;
//   config.set('stat.zoom', stat.zoom);
//   console.log(config.path, stat.zoom);
// })

// document.querySelector('#reset').addEventListener('click', () => {
//   stat.zoom = 100;
//   changeZoom(stat.zoom);
//   document.querySelector('#zoom').value = stat.zoom;
// })

// function changeZoom(zoom) {
//   statStyles.innerHTML = `div.statmessage { font-size: ${zoom}% }`;
// }

// document.querySelector('#zoom').addEventListener('input', () => {
//   console.log('zoom', document.querySelector('#zoom').value )
//   stat.zoom = document.querySelector('#zoom').value;
//   changeZoom(stat.zoom);
// })


document.querySelector('#metric').addEventListener('click', () => {
  settings.metric = true;
  config.set('stat.metric', true)
  
  document.querySelector('#metric').className = "on";
  document.querySelector('#imperial').className = "off";
  
  document.querySelector('#stat').innerHTML = renderStat(lastStat, settings.metric);
})

document.querySelector('#imperial').addEventListener('click', () => {
  settings.metric = false;
  config.set('stat.metric', false)
  
  document.querySelector('#metric').className = "off"
  document.querySelector('#imperial').className = "on"
  
  document.querySelector('#stat').innerHTML = renderStat(lastStat, settings.metric);
})




// ---

window.addEventListener('load', () => {

  if (settings.metric) {
    document.querySelector('#metric').className = "on"
    document.querySelector('#imperial').className = "off"
  } else {
    document.querySelector('#metric').className = "off"
    document.querySelector('#imperial').className = "on"
  }
  
  document.querySelector('#stat').innerHTML = renderStat(lastStat, settings.metric);

//     var elems = document.querySelectorAll('[data-help]')
//     // var values =
//     elems.forEach( (obj) => {
//       // alert(obj.innerHTML);
//       hpfy.__(obj);
//       // return obj.value;
//   });
})

window.addEventListener('beforeunload', () => {
  saveConfig();
})


function round(number, precision) {
    var factor = Math.pow(10, precision);
    var tempNumber = number * factor;
    var roundedTempNumber = Math.round(tempNumber);
    return roundedTempNumber / factor;
};

function timestamp(sec) {
  rest = sec
  rest -= (hh = Math.floor(rest/60/60))*60*60
  rest -= (mm = Math.floor(rest/60))*60;
  ss = rest

  hh = "00" + hh
  mm = "00" + mm
  ss = "00" + ss
  
  return hh.slice(-2) + ":" + mm.slice(-2) + ":" + ss.slice(-2)
}

function runningCadence(speed) {
  if (speed == 0) return "-:--";

  kph = speed / 1000000;
  secperkm = (60*60) / kph;

  mm = Math.floor(secperkm/60)
  ss = Math.floor(secperkm - mm*60)

  return mm + ":" + ("00" + ss).slice(-2)
}

function readConfig() {
  var settings = { show: {} }


  console.log(config.has('stat.show'), config.has('stat.show.speed'), config.has('stat.show.units'))

  fields.forEach( (field) => {
    settings.show[field] = (config.has('stat.show.'+field) ? (config.get('stat.show.'+field)) : true);
    console.log((config.has('stat.show.'+field) ? (config.get('stat.show.'+field)) : true))
    document.querySelector('input[data-show="' + field + '"').checked = settings.show[field]
  })

  settings.show.units = (config.has('stat.show.units') ? (config.get('stat.show.units')) : true)
  document.querySelector('input[data-show="units"').checked = settings.show.units
  if (settings.show.units) {
    $('#units').show()
  } else {
    $('#units').hide()
  }


  settings.metric = (config.has('stat.metric') ? (config.get('stat.metric')) : true)

  return settings;
}

function saveConfig() {

  config.set('stat.show', {}) 

  fields.forEach( (field) => {
    config.set('stat.show.'+field, settings.show[field]) 
  })

  config.set('stat.show.units', settings.show.units)

  config.set('stat.metric', settings.metric)
  
}